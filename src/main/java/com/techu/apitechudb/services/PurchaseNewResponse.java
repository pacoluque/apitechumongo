package com.techu.apitechudb.services;

import com.techu.apitechudb.models.PurchaseModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Clase que encapsula respuesta para la operación de creación de una nueva compra
 */
public class PurchaseNewResponse {

    /** Indica si el usuario al que se le va a asignar la compra existe */
    private boolean usuarioExiste;

    /** Indica si el identificador de la compra no existe previamente */
    private boolean compraNoExistePreviamente;

    /** Lista que contiene los identificadores de los productos no encontrados. Lista vacía si todos los productos fueron encontrados */
    private List<String> productosNoEncontrados;

    /** Objeto que encapsula los datos de la compra una vez realizada */
    private PurchaseModel purchaseModel;


    public PurchaseNewResponse(boolean usuarioExiste, boolean compraNoExistePreviamente, List<String> productosNoEncontrados, PurchaseModel purchaseModel) {
        this.usuarioExiste = usuarioExiste;
        this.compraNoExistePreviamente = compraNoExistePreviamente;
        this.productosNoEncontrados = productosNoEncontrados;
        this.purchaseModel = purchaseModel;
    }

    public boolean isDonePurchase(){
        if (usuarioExiste && compraNoExistePreviamente && productosNoEncontrados.isEmpty() && purchaseModel != null)
            return true;
        else
            return false;
    }

    public boolean isUsuarioExiste() {
        return usuarioExiste;
    }

    public void setUsuarioExiste(boolean usuarioExiste) {
        this.usuarioExiste = usuarioExiste;
    }

    public boolean isCompraNoExistePreviamente() {
        return compraNoExistePreviamente;
    }

    public void setCompraNoExistePreviamente(boolean compraNoExistePreviamente) {
        this.compraNoExistePreviamente = compraNoExistePreviamente;
    }

    public List<String> getProductosNoEncontrados() {
        return productosNoEncontrados;
    }

    public void setProductosNoEncontrados(List<String> productosNoEncontrados) {
        this.productosNoEncontrados = productosNoEncontrados;
    }

    public PurchaseModel getPurchaseModel() {
        return purchaseModel;
    }

    public void setPurchaseModel(PurchaseModel purchaseModel) {
        this.purchaseModel = purchaseModel;
    }
}
