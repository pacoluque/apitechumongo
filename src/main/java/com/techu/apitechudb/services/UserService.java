package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.ProductRepository;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.tags.form.SelectTag;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Clase de negocio para operar con usuarios
 */
@Service
public class UserService {

    /* Interfaz para trabajar contra el repositorio de usuarios */
    @Autowired
    UserRepository userRepository;

    /**
     * Añade un nuevo usuario al repositorio
     * @param userModel Objeto que encapsula los datos el usuario a insertar
     * @return Objeto que encapsula los datos el usuario insertado
     */
    public UserModel addUser(UserModel userModel){
        System.out.println("addUser en UserService");
        return this.userRepository.insert(userModel);
    }


    /**
     * Recupera todos los usuarios del repositorio con la posiblidad de ordenar por un campo dado y en orden ascendente o descendente
     * @param campoOrdenacion Campo por el cual se quiere ordenar. Puede ser nulo si no se quiere ordenar por ningún campo.
     * @param sentidoOrdenacion ASC: Orden ascendente | DESC: Orden descendente. Puede ser nulo si no se quiere ordenar en ningún sentido explícito.
     * @return Una lista de objetos UserModel que encapsulan los resultados de la búsqueda (ordenados en base al criterio establecido si este existe)
     */
    public List<UserModel> findAll(String campoOrdenacion, String sentidoOrdenacion){
        System.out.println("findAll en UserService");
        System.out.println("orderBy en UserService tiene un valor: "+campoOrdenacion);

        List<UserModel> result;

        if (campoOrdenacion==null){ //NO hay criterios de ordenación
            result = this.userRepository.findAll();
        }
        else{ // Hay criterios de ordenación
            Sort.Order orderCriteria;

            if (sentidoOrdenacion != null && (sentidoOrdenacion.equalsIgnoreCase("ASC")))
                orderCriteria = new Sort.Order(Sort.Direction.ASC, campoOrdenacion);
            else if ((sentidoOrdenacion != null && (sentidoOrdenacion.equalsIgnoreCase("DESC"))))
                orderCriteria = new Sort.Order(Sort.Direction.DESC, campoOrdenacion);
            else
                orderCriteria = new Sort.Order(null, campoOrdenacion);

            result = this.userRepository.findAll(Sort.by(orderCriteria));
        }

        return result;
    }

    /**
     * Elimina un usuario dado su identificador
     * @param id Identificador del usuario a eliminar
     * @return Un objeto Optional que almacenara un objeto UserModel si el usuario existe ha posido ser eliminado. En caso contrario el objeto almacenado será nulo.
     */
    public Optional<UserModel> deleteUserById(String id){
        System.out.println("deleteUser en UserService");
        Optional<UserModel> result = this.userRepository.findById(id);

        if (result.isPresent())
            this.userRepository.deleteById(id);

        return result;
    }

    /**
     * Actualiza todos los datos de un usuario en base a su identificador
     * @param user Objeto que encapsula los datos del usuario a actualizar
     * @return Un objeto Optional que almacenara un objeto UserModel si el usuario ha posido ser actualizado. En caso contrario el objeto almacenado será nulo.
     */
    public Optional<UserModel> updateUserById(UserModel user) {
        System.out.println("updateUserById en usertService");
        Optional<UserModel> result = this.userRepository.findById(user.getId());

        if (result.isPresent()) {
            this.userRepository.save(user);

            result.get().setId(user.getId());
            result.get().setName(user.getName());
            result.get().setAge(user.getAge());
        }

        return result;
    }

    /**
     * Recupera los datos de un usuario dado su identificador
     * @param id Identificador del usuario a recuperar
     * @return Un objeto Optional que almacenará un objeto UserModel si el usuario ha posido ser recuperado. En caso contrario el objeto almacenado será nulo.
     */
    public Optional<UserModel> getUserById(String id){
        System.out.println("getUserById en USerService");
        return this.userRepository.findById(id);
    }
}
