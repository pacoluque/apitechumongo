package com.techu.apitechudb.services;


import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Clase de negocio para operar con compras
 */
@Service
public class PurchaseService {

    /* Interfaz para trabajar contra el repositorio de compras */
    @Autowired
    PurchaseRepository purchaseRepository;

    /* Servicio de negocio para operar con el recurso "User" */
    @Autowired
    UserService userService;

    /* Servicio de negocio para operar con el recurso "Product" */
    @Autowired
    ProductService productService;

    /**
     * Añade una nueva compra al repositorio
     * @param purchaseModel Objeto que encapsula los datos de la compra a insertar
     * @return Objeto PurchaseNewResponse que contiene el objeto PurchaseModel con la compra creada y todas las variables que indican el éxito o no de la operación.
     */
    public PurchaseNewResponse addPurchase(PurchaseModel purchaseModel){
        System.out.println("addPurchase en PurchaseService");

        float importeTotal = 0;
        List<String> listaProductosNoEncontrados = new ArrayList();
        PurchaseNewResponse purchaseNewResponse = new PurchaseNewResponse(true,true, listaProductosNoEncontrados, null );


        /** Arrancamos validaciones. Esto habría que refactorizarlo para hacerlo más eficiente */
        /** 1. Verificamos que el ID del usuario exista en el sistema */
        Optional optionalUser = userService.getUserById(purchaseModel.getUserId());
        if (optionalUser.isEmpty())  // El usuario NO existe
            purchaseNewResponse.setUsuarioExiste(false); // Actualizamos valor para la respuesta


        /** 2. Verificamos que los productos existen y aprovechamos para calcular el importe total */
        Set<String> set = purchaseModel.getPurchaseItems().keySet();
        Optional<ProductModel> optionalProduct;
        for (String idProduct : set) { // Comprobamos que existen todos los productos en base de datos
            optionalProduct = productService.getProductById(idProduct);
            if (optionalProduct.isEmpty())
                listaProductosNoEncontrados.add(idProduct);// Este producto no existe
            else {
                importeTotal = importeTotal + ((Integer)purchaseModel.getPurchaseItems().get(idProduct)).intValue() * optionalProduct.get().getPrice(); // Calculamos total
            }
        }

        //purchaseNewResponse.setProductosNoEncontrados(listaProductosNoEncontrados); // Actualizamos valor para la respuesta

        /** 3. Verificamos que la compra no existe previamente */
        Optional<PurchaseModel> optionalPurchase = purchaseRepository.findById(purchaseModel.getId());
        if (optionalPurchase.isPresent())
            purchaseNewResponse.setCompraNoExistePreviamente(false); // Actualizamos valor para la respuesta
            //compraExistePreviamente = true;

        // Si todas las validaciones ha sido superadas
        if (purchaseNewResponse.isUsuarioExiste() && purchaseNewResponse.getProductosNoEncontrados().isEmpty() && purchaseNewResponse.isCompraNoExistePreviamente()) {
            purchaseModel.setAmount(importeTotal);
            purchaseNewResponse.setPurchaseModel(purchaseRepository.insert(purchaseModel));
        }

        return purchaseNewResponse;
    }


    /**
     * Recupera todas las compras
     * @return La lista de objetos PurchaseModel que existen en el sistema
     */
    public List<PurchaseModel> findAll(){
        System.out.println("findAll en PurchaseService");
        return this.purchaseRepository.findAll();
    }
}
