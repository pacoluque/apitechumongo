package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Clase de negocio para operar con productos
 */
@Service
public class ProductService {

    /** Interfaz para trabajar contra el repositorio de productos */
    @Autowired
    ProductRepository productRepository;

    /**
     * Recupera todos los productos
     * @return La lista de objetos ProductModel que existen en el sistema
     */
    public List<ProductModel> findAll(){
        System.out.println("findAll en ProductService");

        //Con MongoDB instalado:
        return this.productRepository.findAll();
    }


    /**
     * Añade un nuevo producto al repositorio
     * @param productModel Objeto que encapsular los datos del producto a añadir
     * @return Objeto que encapsula los datos el producto insertado
     */
    public ProductModel addProduct(ProductModel productModel){
        System.out.println("addProduct en ProductService");
        return this.productRepository.insert(productModel);
    }


    /**
     * Recupera los datos de un producto dado su identificador
     * @param id Identificador del producto a recuperar
     * @return Un objeto Optional que almacenará un objeto ProductModel si el producto ha posido ser recuperado. En caso contrario el objeto almacenado será nulo.
     */
    public Optional<ProductModel> getProductById(String id){
        System.out.println("getProductById en ProductService");
        return this.productRepository.findById(id);
    }

    /**
     * Elimina un producto dado su identificador
     * @param id Identificador del producto a eliminar
     * @return Un objeto Optional que almacenara un objeto ProductModel si el producto existe y ha posido ser eliminado. En caso contrario el objeto almacenado será nulo.
     */
    public Optional<ProductModel> deleteProductById(String id){
        System.out.println("deleteProduct en ProductService");
        Optional<ProductModel> result = this.productRepository.findById(id);

        if (result.isPresent())
            this.productRepository.deleteById(id);

        return result;
    }

    /**
     * Actualiza un producto en base al identificador del mismo
     * @param product Producto a actualizar
     * @return Un objeto Optional que almacenara un objeto ProductModel si el producto ha posido ser actualizado. En caso contrario el objeto almacenado será nulo.
     */
    public Optional<ProductModel> updateProductById(ProductModel product) {
        System.out.println("updateProduct en ProductService");
        Optional<ProductModel> result = this.productRepository.findById(product.getId());

        //this.productRepository.save(product);

        if (result.isPresent()) {
            //this.productRepository.deleteById(product.getId());
            //this.productRepository.insert(product);
            this.productRepository.save(product);

            result.get().setId(product.getId());
            result.get().setDesc(product.getDesc());
            result.get().setPrice(product.getPrice());
        }

        return result;
    }
}
