package com.techu.apitechudb.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Map;

/**
 * Clase que representa a la entidad/recurso "Compra" en el modelo del sistema
 */
@Document(collection = "purchases")
public class PurchaseModel {

    /** Identificador de la compra */
    @Id
    private String id;

    /** Identificador del usuario que realiza la compra */
    private String userId;

    /** Total de la compra */
    private float amount;

    /** Colección que contiene los productos comprados y la cantidad de cada uno de ellos */
    private Map<String, Integer> purchaseItems;

    public PurchaseModel() {
    }

    public PurchaseModel(String id, String userId, float amount, Map purchaseItems) {
        this.id = id;
        this.userId = userId;
        this.amount = amount;
        this.purchaseItems = purchaseItems;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public Map getPurchaseItems() {
        return purchaseItems;
    }

    public void setPurchaseItems(Map purchaseItems) {
        this.purchaseItems = purchaseItems;
    }
}
