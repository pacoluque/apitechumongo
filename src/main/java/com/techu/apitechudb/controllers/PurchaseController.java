package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.PurchaseNewResponse;
import com.techu.apitechudb.services.PurchaseService;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * Clase controladora para el recurso "purchase"
 */
@RestController
@RequestMapping("/apitechu/V2")
public class PurchaseController {

    /* Servicio de negocio para operar con el recurso "Purchase" */
    @Autowired
    PurchaseService purchaseService;


    /**
     * Crea una nueva compra
     * @param purchaseModel Objeto que encapsula los datos de la compra a crear
     * @return Código HTTP 200 - JSON con los datos de la compra que ha sido creada
     *         Código HTTP 409 - Cuando no se superan las validaciones de negocio para crear la compra
     *         Código HTTP 400 - Cuando los datos recibidos no son correctos para realizar la operación
     */
    @PostMapping("/purchases")
    public ResponseEntity<HttpMessage> addPurchase(@RequestBody PurchaseModel purchaseModel){
        System.out.println("addPurchase...");
        System.out.println("El id de la compra a crear "+purchaseModel.getId());

        ResponseEntity<HttpMessage> responseEntity;
        PurchaseNewResponse purchaseNewResponse;
        HttpMessage httpMessage;

        /** Validaciones de datos de entrada */
        List erroresDatosEntrada = this.validadorDatosEntradaNewPurchase(purchaseModel);

        /** Los datos de entrada son correctos y procedemos con el alta.
         * Si se producen errores se van recogiendo para ajustar la salida al consumidor */
        if (erroresDatosEntrada.isEmpty()) {
            purchaseNewResponse = this.purchaseService.addPurchase(purchaseModel);
            if (purchaseNewResponse.isDonePurchase()) // La compra se ha realizado
                httpMessage = new HttpMessage(String.valueOf(System.currentTimeMillis()), HttpStatus.CREATED, "Compra dada de alta en el sistema", "/apitechu/V2/purchases", purchaseNewResponse.getPurchaseModel());
            else { // Examinamos los errores al realizar la compra para armar el mensaje de salida oportuno
                List erroresEncontrados = new ArrayList();
                if (!purchaseNewResponse.isUsuarioExiste()) // El usuario no existe
                    erroresEncontrados.add("Error: Usuario " + purchaseModel.getUserId() + " no encontrado en el sistema");

                if (!purchaseNewResponse.isCompraNoExistePreviamente()) // La compra YA existe
                    erroresEncontrados.add("Error: ID compra " + purchaseModel.getId() + " ya existe en el sistema");

                if (!purchaseNewResponse.getProductosNoEncontrados().isEmpty()) // Hay productos que no existen
                    for (String prodNoEncontrado : purchaseNewResponse.getProductosNoEncontrados())
                        erroresEncontrados.add("Error: Producto con identificador " + prodNoEncontrado + " no existe en el sistema");

                httpMessage = new HttpMessage(String.valueOf(System.currentTimeMillis()), HttpStatus.CONFLICT, "La compra no ha podido realizarse", "/apitechu/V2/purchases", erroresEncontrados);
            }
        }
        else // Datos de entrada incorrectos
            httpMessage = new HttpMessage(String.valueOf(System.currentTimeMillis()), HttpStatus.BAD_REQUEST,"Datos de entrada incorrectos", "/apitechu/V2/purchases", null);


        return new ResponseEntity(httpMessage, httpMessage.getStatus());
    }


    /**
     * Obtiene todas las compras
     *
     * @return Código HTTP 200 - Una lista de objetos JSON que contienen los datos de cada compra.
     */
    @GetMapping("/purchases")
    public ResponseEntity<HttpMessage> getPurchases(){
        System.out.println("getPurchases...");
        List listaCompras = this.purchaseService.findAll();

        HttpMessage httpMessage = new HttpMessage(String.valueOf(System.currentTimeMillis()), HttpStatus.OK, "Operacion realizada con éxito", "/apitechu/V2/purchases", listaCompras);

        return new ResponseEntity<HttpMessage>(httpMessage, httpMessage.status);
    }


    /**
     * Validador para los datos de entrada de una nueva compra
     * @param purchaseModel
     * @return Una lista vacia si no hay errores o una lista con los errores encontrados
     */
    private List validadorDatosEntradaNewPurchase(PurchaseModel purchaseModel){

        List<String> errores = new ArrayList();

        if(purchaseModel!=null) // Existe el objeto PurchaseModel
            if(purchaseModel.getPurchaseItems()!=null)
                if (!purchaseModel.getPurchaseItems().isEmpty()) { // La lista de productos no está vacía
                    if (purchaseModel.getUserId() == null)  // El identificador de usuario NO viene informado
                        errores.add("Usuario no informado");
                }
                else
                    errores.add("Lista de artículos de la compra está vacía");
            else
                errores.add("No existe la lista de artículos de la compra ");
        else
            errores.add("Datos para la nueva compra no informados");

        return errores;
    }
}
