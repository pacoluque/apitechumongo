package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * clase controladora para el recurso Product
 */
@RestController
@RequestMapping("/apitechu/V2")
public class ProductController {

    /* Servicio de negocio para operar con el recurso "Product" */
    @Autowired
    ProductService productService;

    /**
     * Obtiene todos los productos
     *
     * @return Código HTTP 200 - Una lista de objetos JSON que contienen los datos de cada producto.
     */
    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts(){

        System.out.println("getProducts...");
        ResponseEntity<List<ProductModel>> listResponseEntity = new ResponseEntity<>(this.productService.findAll(), HttpStatus.OK);

        if (listResponseEntity.hasBody())
            System.out.println("Hay resultados");

        return listResponseEntity;
    }


    /**
     * Crea un nuevo producto
     * @param product Objeto que encapsula los datos del producto a crear
     * @return Código HTTP 200 - JSON con los datos del producto que ha sido creado
     */
    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product){
        System.out.println("addProduct...");
        System.out.println("El id del producto a crear "+product.getId());

        ResponseEntity<ProductModel> responseEntity = new ResponseEntity<>(this.productService.addProduct(product), HttpStatus.CREATED);

        return responseEntity;
       //return this.productService.addProduct(product);
    }

    /**
     * Recupera los datos de un producto dado su identificador
     * @param id Identificador del producto a recuperar
     * @return Código HTTP 200 - JSON con los datos del producto cuyo identificador coincide con el facilitado.
     *         Código HTTP 401 - Cuando el recurso usuario no ha sido encontrado
     */
    @GetMapping("/products/{id}")
    public ResponseEntity getProductById(@PathVariable String id){

        System.out.println("getProductById");
        System.out.println("Id es:"+id);

        ResponseEntity<ProductModel> responseEntity;

        Optional<ProductModel> result = this.productService.getProductById(id);


        // El profesor lo hace devolviendo siempre un productModel vacío.
        if (result.isEmpty())
            responseEntity = new ResponseEntity("Producto no encontrado", HttpStatus.NOT_FOUND);
        else
            responseEntity = new ResponseEntity(result.get(), HttpStatus.OK);


        return responseEntity;
    }

    /**
     * Elimina un producto dado su identificador
     * @param id Identificador del producto a eliminar
     * @return Código HTTP 200 - JSON con los datos del producto que ha sido eliminado
     *         Código HTTP 401 - Cuando el producto a eliminar no ha sido encontrado
     */
    @DeleteMapping("/products/{id}")
    public ResponseEntity deleteProduct(@PathVariable String id){
        System.out.println("deleteProduct...");
        System.out.println("El id del producto a eliminar "+id);

        ResponseEntity<ProductModel> responseEntity;
        Optional<ProductModel> result = this.productService.deleteProductById(id);

        if (result.isEmpty())
            responseEntity = new ResponseEntity("Producto no encontrado", HttpStatus.NOT_FOUND);
        else
            responseEntity = new ResponseEntity(result.get(), HttpStatus.OK);


        return responseEntity;
        //return this.productService.addProduct(product);
    }

    /**
     * Actualiza un producto dado su identificador.
     * @param product Datos del producto a actualizar
     * @param id Identificador del producto a actualizar
     * @return Código HTTP 200 - JSON con los datos del producto que han sido actualizado
     *         Código HTTP 401 - Cuando el producto no ha sido encontrado
     *         Código HTTP 409 - Cuando el identificador proporcionado en URL no coincide con el identificador informado en el body de la petición
     *
     */
    @PutMapping("/products/{id}")
    public ResponseEntity updateProduct(@RequestBody ProductModel product, @PathVariable String id){
        System.out.println("updateProduct...");
        System.out.println("El id del producto a actualizar es: "+id);
        System.out.println("La descripción del producto a actualizar es: "+product.getDesc());

        ResponseEntity responseEntity;

        if (!product.getId().equals(id)) {
            System.out.println("ID de URL y body no coinciden");
            responseEntity = new ResponseEntity("IDs no coinciden", HttpStatus.CONFLICT);
        }
        else{
            Optional<ProductModel> result = this.productService.updateProductById(product);

            if (result.isEmpty())
                responseEntity = new ResponseEntity("Producto no encontrado", HttpStatus.NOT_FOUND);
            else
                responseEntity = new ResponseEntity<>(result.get(), HttpStatus.OK);
        }

        return responseEntity;
        //return this.productService.addProduct(product);
    }
}
