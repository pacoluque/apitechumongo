package com.techu.apitechudb.controllers;

import org.springframework.http.HttpStatus;

/**
 * Clase para encapsular los mensajes de salida hacia el consumidor HTTP
 */
public class HttpMessage{

    String timestamp;
    HttpStatus status;
    String message;
    String path;
    Object additionalInfo;

    public HttpMessage(String timestamp, HttpStatus status,  String message, String path, Object additionalInfo) {
        this.timestamp = timestamp;
        this.status = status;
        this.message = message;
        this.path = path;
        this.additionalInfo = additionalInfo;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public HttpStatus getStatus() {
        return status;
    }


    public String getMessage() {
        return message;
    }

    public String getPath() {
        return path;
    }

    public Object getadditionalInfo() {
        return additionalInfo;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setRecurso(Object additionalInfo) {
        this.additionalInfo = additionalInfo;
    }
}

