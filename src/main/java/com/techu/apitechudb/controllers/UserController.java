package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


/**
 * Clase controladora para el recurso "user"
 */
@RestController
@RequestMapping("/apitechu/V2")
public class UserController {

    /* Servicio de negocio para operar con el recurso "User" */
    @Autowired
    UserService userService;

    /**
     * Obtiene todos los usuarios posibilitando la ordenación por un campo dado
     * @param orderBy Campo por el cual se quiere ordenar. Sigue estándar ODATA. OPCIONAL
     * @return Código HTTP 200 - Una lista de objetos JSON que contienen los datos de cada usuario.
     */
    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(value = "$orderBy", required = false) String orderBy) {
        System.out.println("getUsers..");
        System.out.println("El campo por el que hay que ordenar es: "+orderBy);

        String campoOrdenacion = null;
        String sentidoOrdenacion = null;

        // Extreamos los criterios de ordenación en el caso de existan
        if (orderBy != null) {
            String[] campos = orderBy.split(" ");
            System.out.println("El número de elementos que vienen en orderBy es "+campos.length+" elementos");
            campoOrdenacion = campos[0];
            sentidoOrdenacion = (campos.length > 1) ? campos[1] : null;
        }

        ResponseEntity<List<UserModel>> listResponseEntity = new ResponseEntity<>(this.userService.findAll(campoOrdenacion, sentidoOrdenacion), HttpStatus.OK);

        if (listResponseEntity.hasBody())
            System.out.println("Hay resultados");

        return listResponseEntity;
    }

    /**
     * Crea un nuevo usuario
     * @param user Objeto que encapsula los datos del usuario a crear
     * @return Código HTTP 200 - JSON con los datos del usuario que ha sido creado
     */
    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("addUser...");
        System.out.println("El id del usuario a crear "+user.getId());

        ResponseEntity<UserModel> responseEntity = new ResponseEntity<>(this.userService.addUser(user), HttpStatus.CREATED);

        return responseEntity;
    }

    /**
     * Elimina los datos de un usuario dado su identificador
     * @param id Identificador del usuario a eliminar
     * @return Código HTTP 200 - JSON con los datos del usuario que ha sido eliminado
     *         Código HTTP 401 - Cuando el recurso usuario no ha sido encontrado
     */
    @DeleteMapping("/users/{id}")
    public ResponseEntity deleteUser(@PathVariable String id){
        System.out.println("deleteUser...");
        System.out.println("El id del usuario a eliminar "+id);

        ResponseEntity<UserModel> responseEntity;
        Optional<UserModel> result = this.userService.deleteUserById(id);

        if (result.isEmpty())
            responseEntity = new ResponseEntity("Usuario no encontrado", HttpStatus.NOT_FOUND);
        else
            responseEntity = new ResponseEntity(result.get(), HttpStatus.OK);


        return responseEntity;
    }

    /**
     * Actualiza todos los datos de un usuario en base al identificador proporcionado
     * @param user Objeto que encapsula los datos del usuario a actualizar
     * @param id Identificador del usuario a actualizar
     * @return Código HTTP 200 - JSON con los datos del usuario que han sido actualizado
     *         Código HTTP 401 - Cuando el recurso usuario no ha sido encontrado
               Código HTTP 409 - Cuando el identificador proporcionado en URL no coincide con el identificador informado en el body de la petición
     */
    @PutMapping("/users/{id}")
    public ResponseEntity updateUser(@RequestBody UserModel user, @PathVariable String id){
        System.out.println("updateUser...");
        System.out.println("El id del usuario a actualizar es: "+id);
        System.out.println("El nombre del usuario a actualizar es: "+user.getName());

        ResponseEntity<UserModel> responseEntity;

        if (!user.getId().equals(id)) {
            System.out.println("ID de URL y body no coinciden");
            responseEntity = new ResponseEntity("IDs no coinciden", HttpStatus.CONFLICT);
        }
        else{
            Optional<UserModel> result = this.userService.updateUserById(user);

            if (result.isEmpty())
                responseEntity = new ResponseEntity("Usuario no encontrado", HttpStatus.NOT_FOUND);
            else
                responseEntity = new ResponseEntity(result.get(), HttpStatus.OK);
        }

        return responseEntity;
    }

    /**
     * Recupera los datos de un usuario dado su identificador
     * @param id Identificador del usuario a recuperar
     * @return Código HTTP 200 - JSON con los datos del usuario cuyo identificador coincide con el facilitado
     *         Código HTTP 401 - Cuando el recurso usuario no ha sido encontrado
     */
    @GetMapping("/users/{id}")
    public ResponseEntity getUserById(@PathVariable String id){

        System.out.println("getUserById");
        System.out.println("Id es:"+id);

        ResponseEntity<UserModel> responseEntity;
        Optional<UserModel> result = this.userService.getUserById(id);

        if (result.isEmpty())
            responseEntity = new ResponseEntity("Usuario no encontrado", HttpStatus.NOT_FOUND);
        else
            responseEntity = new ResponseEntity(result.get(), HttpStatus.OK);


        return responseEntity;
    }



}
