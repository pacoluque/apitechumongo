package com.techu.apitechudb.controllers;

import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {


    @RequestMapping("/")
    public String index() {
        return "hola mundo amigos desde API Tech U PROYECTO MONGO";
    }


    @RequestMapping("/hello")
    public String greetings(@RequestParam(value = "name", defaultValue = "Tech U") String name) {
        return String.format("hola %s!", name);
    }

}

