package com.techu.apitechudb.repositories;


import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.ProductModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Interfaz para la operación contra el repositorio de datos de productos
 */
@Repository
public interface ProductRepository extends MongoRepository<ProductModel, String> {

}
