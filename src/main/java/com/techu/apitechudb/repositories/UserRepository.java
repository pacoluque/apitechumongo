package com.techu.apitechudb.repositories;

import com.techu.apitechudb.models.UserModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Interfaz para la operación contra el repositorio de datos de usuarios
 */
@Repository
public interface UserRepository extends MongoRepository<UserModel, String> {
}
